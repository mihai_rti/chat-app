#include<iostream>
#include<sstream>
#include<string>
#include<thread>
#include<mutex>
#include<unordered_map>
using namespace std;

#include "chat.h"
#include "chatPlugin.h"
#include "chatSupport.h"


mutex participant_mtx;
DDSDomainParticipant *participant;
DDSGuardCondition *stop_condition = new DDSGuardCondition();;

mutex map_mtx;
struct UserInfo {
    string group;
    string fullName;
};
unordered_map<string, UserInfo> online_users;


int participant_shutdown() {
    return 0;
    participant_mtx.lock();
    if (participant != NULL) {
        participant->delete_contained_entities();
        DDSTheParticipantFactory->delete_participant(participant);
    }
    participant_mtx.unlock();
    return 0;
}

void handle_error(bool error, string msg) {
    if (error) {
        cerr << msg << " error" << endl;
        participant_shutdown();
        exit(-1);
    }
}


int publisher_user_info(DDSTopic* user_topic, string username, string group, string firstName, string lastName)
{
    cout << "___Publisher_user_info thread: started\n";
    DDS_ReturnCode_t retcode;

    participant_mtx.lock();
    DDSPublisher *publisher = participant->create_publisher(DDS_PUBLISHER_QOS_DEFAULT, NULL, DDS_STATUS_MASK_NONE);
    handle_error(publisher == NULL, "create publisher");
    participant_mtx.unlock();

    DDSDataWriter *writer = publisher->create_datawriter_with_profile(user_topic,
            "chat_library", "user_info_profile", NULL, DDS_STATUS_MASK_NONE);
    handle_error(writer == NULL, "create datawriter");
    ChatUserDataWriter *chat_writer = ChatUserDataWriter::narrow(writer);
    handle_error(writer == NULL, "narrow datawriter");

    ChatUser *instance = ChatUserTypeSupport::create_data();
    handle_error(instance == NULL, "create data");

    strncpy(instance->username, username.c_str(), MAX_STR_SIZE);
    strncpy(instance->group, group.c_str(), MAX_STR_SIZE);
    instance->firstName = DDS_String_dup(firstName.c_str());
    instance->lastName = DDS_String_dup(lastName.c_str());
    
    retcode = chat_writer->write(*instance, DDS_HANDLE_NIL);
    handle_error(retcode != DDS_RETCODE_OK, "write");

    DDS_Duration_t wait_period = { 5, 0 };
    // wake up based on guard condition
    while (true) {
        NDDSUtility::sleep(wait_period);
        if (stop_condition->get_trigger_value() == true) {
            retcode = chat_writer->unregister_instance(*instance, DDS_HANDLE_NIL);
            handle_error(retcode != DDS_RETCODE_OK, "unregister");
            break;
        }
    }
    retcode = ChatUserTypeSupport::delete_data(instance);
    handle_error(retcode != DDS_RETCODE_OK, "delete data");
    
    cout << "___Publisher_user_info thread: finished\n";
    return 0;
}


int publisher_main(DDSTopic* message_topic, string username, string group)
{
    cout << "___Publisher_main thread: started\n";
    DDS_ReturnCode_t retcode;

    participant_mtx.lock();
    // add partitions for publisher
    DDS_PublisherQos publisher_qos;
    participant->get_default_publisher_qos(publisher_qos);
    publisher_qos.partition.name.ensure_length(1, 1);
    publisher_qos.partition.name[0] = DDS_String_dup(group.c_str());
    
    DDSPublisher *publisher = participant->create_publisher(publisher_qos, NULL, DDS_STATUS_MASK_NONE);
    handle_error(publisher == NULL, "create publisher");
    participant_mtx.unlock();

    DDSDataWriter *writer = publisher->create_datawriter_with_profile(message_topic, 
            "chat_library", "message_profile", NULL, DDS_STATUS_MASK_NONE);
    handle_error(writer == NULL, "create datawriter");
    ChatMessageDataWriter *chat_writer = ChatMessageDataWriter::narrow(writer);
    handle_error(writer == NULL, "narrow datawriter");

    ChatMessage *instance = ChatMessageTypeSupport::create_data();
    handle_error(instance == NULL, "create data");

    while (true) {
        string input, command;
        getline(cin, input);
        
        stringstream ss(input);
        getline(ss, command, ' ');

        if (command == "list") {
            string list;
            map_mtx.lock();
            for (auto& it : online_users)
                list += "# " + it.first + " " + it.second.group + " " + it.second.fullName + "\n";
            map_mtx.unlock();
            cout << list;
        }
        else if (command == "send") {
            string destination;
            string message;

            getline(ss, destination, ' ');
            getline(ss, message, '\n');
            message = username + ": " + message;
            strncpy(instance->message, message.c_str(), MAX_MSG_SIZE);

            map_mtx.lock();
            // destination can be a user or a group
            if (online_users.count(destination)) {
                strncpy(instance->user, destination.c_str(), MAX_STR_SIZE);
                strncpy(instance->group, "NONE", MAX_STR_SIZE);
            } else {
                strncpy(instance->group, destination.c_str(), MAX_STR_SIZE);
                strncpy(instance->user, "NONE", MAX_STR_SIZE);
            }
            map_mtx.unlock();

            retcode = chat_writer->write(*instance, DDS_HANDLE_NIL);
            handle_error(retcode != DDS_RETCODE_OK, "write");

        } else if (command == "exit") {
            stop_condition->set_trigger_value(true);
            break;
        } else {
            cout << "___Unsupported command" << endl;
        }
    }
    retcode = ChatMessageTypeSupport::delete_data(instance);
    handle_error(retcode != DDS_RETCODE_OK, "delete data");
    
    cout << "___Publisher_main thread: finished\n";
    return 0;
}


class BuiltinParticipantListener : public DDSDataReaderListener {
    virtual void on_data_available(DDSDataReader *reader) {
        DDSParticipantBuiltinTopicDataDataReader *builtin_reader = 
                (DDSParticipantBuiltinTopicDataDataReader*) reader;
        DDS_ParticipantBuiltinTopicDataSeq data_seq;
        DDS_SampleInfoSeq info_seq;
        DDS_ReturnCode_t retcode;

        retcode = builtin_reader->take(data_seq, info_seq, DDS_LENGTH_UNLIMITED,
                DDS_ANY_SAMPLE_STATE, DDS_NEW_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
        // not_new view states
        if (retcode == DDS_RETCODE_NO_DATA)
            return;
        
        for (int i = 0; i < data_seq.length(); ++i) {
            if (info_seq[i].valid_data) {
                DDS_PropertySeq property_seq = data_seq[i].property.value;
                string debug_message = "__D__Discovered ";
                DDS_Property_t* p;
                //p = DDSPropertyQosPolicyHelper::lookup_property(data_seq[i].property, "dds.sys_info.username");
                //if (p) debug_message += p->value;
                debug_message += data_seq[i].participant_name.name;
                p = DDSPropertyQosPolicyHelper::lookup_property(data_seq[i].property, "dds.sys_info.hostname");
                if (p) debug_message += "@" + string(p->value);
                p = DDSPropertyQosPolicyHelper::lookup_property(data_seq[i].property, "dds.sys_info.process_id");
                if (p) debug_message += " running chat_app with PID " + string(p->value);
                cout << debug_message << endl;
            }
        }
        builtin_reader->return_loan(data_seq, info_seq);
    }
};


int subscriber_user_info(DDSTopic* user_topic)
{
    cout << "___Subscriber_user_info thread: started\n";
    DDS_ReturnCode_t retcode;
    
    participant_mtx.lock();
    DDSSubscriber *subscriber = participant->create_subscriber(DDS_SUBSCRIBER_QOS_DEFAULT, NULL, DDS_STATUS_MASK_NONE);
    handle_error(subscriber == NULL, "create subscriber");
    participant_mtx.unlock();

    DDSDataReader *reader = subscriber->create_datareader_with_profile(user_topic, 
            "chat_library", "user_info_profile", NULL, DDS_STATUS_MASK_NONE);
    handle_error(reader == NULL, "create reader");
    ChatUserDataReader *chat_reader = ChatUserDataReader::narrow(reader);
    handle_error(reader == NULL, "narrow reader");

    DDSReadCondition *read_condition = reader->create_readcondition(
            DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
    handle_error(read_condition == NULL, "create read condition");

    DDSWaitSet *waitset = new DDSWaitSet();
    retcode = waitset->attach_condition(read_condition);
    handle_error(retcode != DDS_RETCODE_OK, "attach read condition");
    retcode = waitset->attach_condition(stop_condition);
    handle_error(retcode != DDS_RETCODE_OK, "attach guard condition");

    while (true) {
        DDSConditionSeq active_conditions;
        DDS_Duration_t wait_timeout = { 60, 0 };

        retcode = waitset->wait(active_conditions, wait_timeout);
        if (retcode == DDS_RETCODE_TIMEOUT) {
            cerr << "Wait timeout" << endl;
            continue;
        } else if (retcode != DDS_RETCODE_OK) {
            cerr << "Wait returned error " << retcode << endl;
            break;
        }
        for (int c = 0; c < active_conditions.length(); c++) {
            if (active_conditions[c] == read_condition) {
                ChatUserSeq data_seq;
                DDS_SampleInfoSeq info_seq;

                retcode = chat_reader->take_w_condition(data_seq, info_seq, DDS_LENGTH_UNLIMITED, read_condition);
                handle_error(retcode != DDS_RETCODE_OK, "take");

                for (int i = 0; i < data_seq.length(); i++) {
                    if (info_seq[i].valid_data) {
                        UserInfo ui;
                        ui.group = data_seq[i].group;
                        ui.fullName = string(data_seq[i].firstName) + " " + string(data_seq[i].lastName);
                        // Add user to online_users
                        map_mtx.lock();
                        online_users[data_seq[i].username] = ui;
                        map_mtx.unlock();
                        cout << data_seq[i].username << " has entered the chat" << endl;
                    } else {
                        if (info_seq[i].instance_state != DDS_ALIVE_INSTANCE_STATE) {
                            ChatUser *cu = ChatUserTypeSupport::create_data();
                            chat_reader->get_key_value(*cu, info_seq[i].instance_handle);
                            string username = cu->username;
                            cout << username + " has left the chat" << endl;
                            map_mtx.lock();
                            online_users.erase(username);
                            map_mtx.unlock();
                        }
                    }
                }
                chat_reader->return_loan(data_seq, info_seq);
            } else if (active_conditions[c] == stop_condition) {
                return 0;
            }
        }
    }
    cout << "___Subscriber_user_info thread: finished\n";
    return 0;
}


int subscriber_main(DDSTopic* message_topic, string username, string group)
{
    cout << "___Subscriber_main thread: started\n";
    DDS_ReturnCode_t retcode;

    participant_mtx.lock();
    // add partitions for subscriber
    DDS_SubscriberQos subscriber_qos;
    participant->get_default_subscriber_qos(subscriber_qos);
    subscriber_qos.partition.name.ensure_length(1, 1);
    subscriber_qos.partition.name[0] = DDS_String_dup(group.c_str());
    
    DDSSubscriber *subscriber = participant->create_subscriber(subscriber_qos, NULL, DDS_STATUS_MASK_NONE);
    handle_error(subscriber == NULL, "create subscriber");
    participant_mtx.unlock();

    string cft_username = "'" + username + "'";
    string cft_group = "'" + group + "'";
    const char* param_list[] = { cft_username.c_str(), cft_group.c_str() };
    DDS_StringSeq parameters(2);
    parameters.from_array(param_list, 2);
    participant_mtx.lock();
    DDSContentFilteredTopic *cft = participant->create_contentfilteredtopic("Chat_CFT", message_topic,
            "user MATCH %0 or group MATCH %1", parameters);
    handle_error(cft == NULL, "create cft");
    participant_mtx.unlock();

    DDSDataReader *reader = subscriber->create_datareader_with_profile(cft, 
            "chat_library", "message_profile", NULL, DDS_STATUS_MASK_NONE);
    handle_error(reader == NULL, "create reader");
    ChatMessageDataReader *chat_reader = ChatMessageDataReader::narrow(reader);
    handle_error(reader == NULL, "narrow reader");

    DDSReadCondition *read_condition = reader->create_readcondition(
            DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);
    handle_error(read_condition == NULL, "create read condition");

    DDSWaitSet *waitset = new DDSWaitSet();
    retcode = waitset->attach_condition(read_condition);
    handle_error(retcode != DDS_RETCODE_OK, "attach read condition");
    retcode = waitset->attach_condition(stop_condition);
    handle_error(retcode != DDS_RETCODE_OK, "attach guard condition");

    while (true) {
        DDSConditionSeq active_conditions;
        DDS_Duration_t wait_timeout = { 60, 0 };
        
        retcode = waitset->wait(active_conditions, wait_timeout);
        if (retcode == DDS_RETCODE_TIMEOUT) {
            cerr << "Wait timeout" << endl;
            continue;
        } else if (retcode != DDS_RETCODE_OK) {
            cerr << "Wait returned error " << retcode << endl;
            break;
        }
        for (int c = 0; c < active_conditions.length(); c++) {
            if (active_conditions[c] == read_condition) {
                ChatMessageSeq data_seq;
                DDS_SampleInfoSeq info_seq;

                retcode = chat_reader->take_w_condition(data_seq, info_seq, DDS_LENGTH_UNLIMITED, read_condition);
                handle_error(retcode != DDS_RETCODE_OK, "take");

                for (int i = 0; i < data_seq.length(); i++)
                    if (info_seq[i].valid_data)
                        cout << data_seq[i].message << endl;
                chat_reader->return_loan(data_seq, info_seq);                

            } else if (active_conditions[c] == stop_condition) {
                return 0;
            }
        }
    }
    cout << "___Subscriber_main thread: finished\n";
    return 0;
}

void enable_debug_info()
{
    participant_mtx.lock();
    // get builtin subscriber for debug info
    DDSSubscriber *builtin_subscriber = participant->get_builtin_subscriber();
    handle_error(builtin_subscriber == NULL, "get builtin subscriber");
    
    DDSParticipantBuiltinTopicDataDataReader *builtin_participant_datareader =
        (DDSParticipantBuiltinTopicDataDataReader*)builtin_subscriber->lookup_datareader(DDS_PARTICIPANT_TOPIC_NAME);
    handle_error(builtin_participant_datareader == NULL, "get builtin datareader");
    
    BuiltinParticipantListener *builtin_participant_listener = new BuiltinParticipantListener();
    builtin_participant_datareader->set_listener(builtin_participant_listener, DDS_DATA_AVAILABLE_STATUS);
    
    // enable participant after listener is installed
    DDS_ReturnCode_t retcode = participant->enable();
    handle_error(retcode != DDS_RETCODE_OK, "enable participant");
    participant_mtx.unlock();
}


int main(int argc, char* argv[])
{
    string username, group;
    string firstName, lastName;
    const int domain_id = 10;
    DDS_ReturnCode_t retcode;

    if (argc < 3) {
        cerr << "Usage: chat_publisher username group [firstName] [lastName]" << endl;
        return -1;
    }
    username = string(argv[1]);
    group = string(argv[2]);
    if (argc >= 4)
        firstName = string(argv[3]);
    if (argc >= 5)
        lastName = string(argv[4]);

    // load custom QoS file
    DDS_DomainParticipantFactoryQos factory_qos;
    DDSTheParticipantFactory->get_qos(factory_qos);
    factory_qos.profile.url_profile.ensure_length(1, 1);
    factory_qos.profile.url_profile[0] = DDS_String_dup("chat_qos.xml");
    DDSTheParticipantFactory->set_qos(factory_qos);

    // set participant_name to username
    DDS_DomainParticipantQos participant_qos;
    retcode = DDSTheParticipantFactory->get_participant_qos_from_profile(participant_qos,
            "chat_library", "user_info_profile");
    handle_error(retcode != DDS_RETCODE_OK, "get qos");
    participant_qos.participant_name.name = DDS_String_dup(username.c_str());
    // create global domain participant
    participant = DDSTheParticipantFactory->create_participant(domain_id, participant_qos, NULL, DDS_STATUS_MASK_NONE);
    handle_error(participant == NULL, "create participant");

    // create topic for user info
    const char* user_type_name = ChatUserTypeSupport::get_type_name();
    retcode = ChatUserTypeSupport::register_type(participant, user_type_name);
    handle_error(retcode != DDS_RETCODE_OK, "register type");
    DDSTopic* user_topic = participant->create_topic(user_topic_name, user_type_name,
            DDS_TOPIC_QOS_DEFAULT, NULL, DDS_STATUS_MASK_NONE);
    handle_error(user_topic == NULL, "create topic");

    // create topic for messages
    const char* message_type_name = ChatMessageTypeSupport::get_type_name();
    retcode = ChatMessageTypeSupport::register_type(participant, message_type_name);
    handle_error(retcode != DDS_RETCODE_OK, "register type");
    DDSTopic* message_topic = participant->create_topic(message_topic_name, message_type_name,
            DDS_TOPIC_QOS_DEFAULT, NULL, DDS_STATUS_MASK_NONE);
    handle_error(message_topic == NULL, "create topic");

    // add listener to builtin topics
    enable_debug_info();

    // create subscriber for user topic
    thread subscriber_thread_user_info(subscriber_user_info, user_topic);
    // create subscriber for message topic
    thread subscriber_thread_message(subscriber_main, message_topic, username, group);
    // create publisher for user topic
    thread publisher_thread_user_info(publisher_user_info, user_topic, username, group, firstName, lastName);

    // create subscriber for message topic
    publisher_main(message_topic, username, group);

    // wait for threads to finish
    publisher_thread_user_info.join();
    subscriber_thread_user_info.join();
    subscriber_thread_message.join();
    
    return participant_shutdown();
}
