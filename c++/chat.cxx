

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from chat.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif 
#ifndef dds_c_log_infrastructure_h                      
#include "dds_c/dds_c_infrastructure_impl.h"       
#endif 

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "chat.h"

#ifndef NDDS_STANDALONE_TYPE
#include "chatPlugin.h"
#endif

#include <new>

/* ========================================================================= */
const char *ChatUserTYPENAME = "ChatUser";

#ifndef NDDS_STANDALONE_TYPE
DDS_TypeCode* ChatUser_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode ChatUser_g_tc_username_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_STR_SIZE)));
    static DDS_TypeCode ChatUser_g_tc_group_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_STR_SIZE)));
    static DDS_TypeCode ChatUser_g_tc_firstName_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_STR_SIZE)));
    static DDS_TypeCode ChatUser_g_tc_lastName_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_STR_SIZE)));

    static DDS_TypeCode_Member ChatUser_g_tc_members[4]=
    {

        {
            (char *)"username",/* Member name */
            {
                0,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_KEY_MEMBER , /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }, 
        {
            (char *)"group",/* Member name */
            {
                1,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }, 
        {
            (char *)"firstName",/* Member name */
            {
                2,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_NONKEY_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }, 
        {
            (char *)"lastName",/* Member name */
            {
                3,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_NONKEY_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }
    };

    static DDS_TypeCode ChatUser_g_tc =
    {{
            DDS_TK_STRUCT, /* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"ChatUser", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            ChatUser_g_tc_members, /* Members */
            DDS_VM_NONE, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER,
            DDS_BOOLEAN_TRUE, /* _isCopyable */
            NULL, /* _sampleAccessInfo: assigned later */
            NULL /* _typePlugin: assigned later */
        }}; /* Type code for ChatUser*/

    if (is_initialized) {
        return &ChatUser_g_tc;
    }

    ChatUser_g_tc._data._annotations._allowedDataRepresentationMask = 5;

    ChatUser_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&ChatUser_g_tc_username_string;
    ChatUser_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&ChatUser_g_tc_group_string;
    ChatUser_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&ChatUser_g_tc_firstName_string;
    ChatUser_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&ChatUser_g_tc_lastName_string;

    /* Initialize the values for member annotations. */
    ChatUser_g_tc_members[0]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
    ChatUser_g_tc_members[0]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

    ChatUser_g_tc_members[1]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
    ChatUser_g_tc_members[1]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

    ChatUser_g_tc._data._sampleAccessInfo =
    ChatUser_get_sample_access_info();
    ChatUser_g_tc._data._typePlugin =
    ChatUser_get_type_plugin_info();    

    is_initialized = RTI_TRUE;

    return &ChatUser_g_tc;
}

#define TSeq ChatUserSeq
#define T ChatUser
#include "dds_cpp/generic/dds_cpp_data_TInterpreterSupport.gen"
#undef T
#undef TSeq

RTIXCdrSampleAccessInfo *ChatUser_get_sample_seq_access_info()
{
    static RTIXCdrSampleAccessInfo ChatUser_g_seqSampleAccessInfo = {
        RTI_XCDR_TYPE_BINDING_CPP, \
        {sizeof(ChatUserSeq),0,0,0}, \
        RTI_XCDR_FALSE, \
        DDS_Sequence_get_member_value_pointer, \
        ChatUserSeq_set_member_element_count, \
        NULL, \
        NULL, \
        NULL \
    };

    return &ChatUser_g_seqSampleAccessInfo;
}

RTIXCdrSampleAccessInfo *ChatUser_get_sample_access_info()
{
    static RTIBool is_initialized = RTI_FALSE;

    ChatUser *sample;

    static RTIXCdrMemberAccessInfo ChatUser_g_memberAccessInfos[4] =
    {RTIXCdrMemberAccessInfo_INITIALIZER};

    static RTIXCdrSampleAccessInfo ChatUser_g_sampleAccessInfo = 
    RTIXCdrSampleAccessInfo_INITIALIZER;

    if (is_initialized) {
        return (RTIXCdrSampleAccessInfo*) &ChatUser_g_sampleAccessInfo;
    }

    RTIXCdrHeap_allocateStruct(
        &sample, 
        ChatUser);
    if (sample == NULL) {
        return NULL;
    }

    ChatUser_g_memberAccessInfos[0].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->username - (char *)sample);

    ChatUser_g_memberAccessInfos[1].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->group - (char *)sample);

    ChatUser_g_memberAccessInfos[2].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->firstName - (char *)sample);

    ChatUser_g_memberAccessInfos[3].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->lastName - (char *)sample);

    ChatUser_g_sampleAccessInfo.memberAccessInfos = 
    ChatUser_g_memberAccessInfos;

    {
        size_t candidateTypeSize = sizeof(ChatUser);

        if (candidateTypeSize > RTIXCdrUnsignedLong_MAX) {
            ChatUser_g_sampleAccessInfo.typeSize[0] =
            RTIXCdrUnsignedLong_MAX;
        } else {
            ChatUser_g_sampleAccessInfo.typeSize[0] =
            (RTIXCdrUnsignedLong) candidateTypeSize;
        }
    }

    ChatUser_g_sampleAccessInfo.useGetMemberValueOnlyWithRef =
    RTI_XCDR_TRUE;

    ChatUser_g_sampleAccessInfo.getMemberValuePointerFcn = 
    ChatUser_get_member_value_pointer;

    ChatUser_g_sampleAccessInfo.languageBinding = 
    RTI_XCDR_TYPE_BINDING_CPP ;

    RTIXCdrHeap_freeStruct(sample);
    is_initialized = RTI_TRUE;
    return (RTIXCdrSampleAccessInfo*) &ChatUser_g_sampleAccessInfo;
}

RTIXCdrTypePlugin *ChatUser_get_type_plugin_info()
{
    static RTIXCdrTypePlugin ChatUser_g_typePlugin = 
    {
        NULL, /* serialize */
        NULL, /* serialize_key */
        NULL, /* deserialize_sample */
        NULL, /* deserialize_key_sample */
        NULL, /* skip */
        NULL, /* get_serialized_sample_size */
        NULL, /* get_serialized_sample_max_size_ex */
        NULL, /* get_serialized_key_max_size_ex */
        NULL, /* get_serialized_sample_min_size */
        NULL, /* serialized_sample_to_key */
        (RTIXCdrTypePluginInitializeSampleFunction) 
        ChatUser_initialize_ex,
        NULL,
        (RTIXCdrTypePluginFinalizeSampleFunction)
        ChatUser_finalize_w_return,
        NULL
    };

    return &ChatUser_g_typePlugin;
}
#endif

RTIBool ChatUser_initialize(
    ChatUser* sample) {
    return ChatUser_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool ChatUser_initialize_ex(
    ChatUser* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return ChatUser_initialize_w_params(
        sample,&allocParams);

}

RTIBool ChatUser_initialize_w_params(
    ChatUser* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (sample == NULL) {
        return RTI_FALSE;
    }
    if (allocParams == NULL) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory) {
        sample->username = DDS_String_alloc(((MAX_STR_SIZE)));
        RTICdrType_copyStringEx(
            &sample->username,
            "",
            ((MAX_STR_SIZE)),
            RTI_FALSE);
        if (sample->username == NULL) {
            return RTI_FALSE;
        }
    } else {
        if (sample->username != NULL) {
            RTICdrType_copyStringEx(
                &sample->username,
                "",
                ((MAX_STR_SIZE)),
                RTI_FALSE);
            if (sample->username == NULL) {
                return RTI_FALSE;
            }
        }
    }

    if (allocParams->allocate_memory) {
        sample->group = DDS_String_alloc(((MAX_STR_SIZE)));
        RTICdrType_copyStringEx(
            &sample->group,
            "",
            ((MAX_STR_SIZE)),
            RTI_FALSE);
        if (sample->group == NULL) {
            return RTI_FALSE;
        }
    } else {
        if (sample->group != NULL) {
            RTICdrType_copyStringEx(
                &sample->group,
                "",
                ((MAX_STR_SIZE)),
                RTI_FALSE);
            if (sample->group == NULL) {
                return RTI_FALSE;
            }
        }
    }

    if (!allocParams->allocate_optional_members) {
        sample->firstName=NULL;
    } else {   

        if (allocParams->allocate_memory) {
            sample->firstName = DDS_String_alloc(((MAX_STR_SIZE)));
            RTICdrType_copyStringEx(
                &sample->firstName,
                "",
                ((MAX_STR_SIZE)),
                RTI_FALSE);
            if (sample->firstName == NULL) {
                return RTI_FALSE;
            }
        } else {
            if (sample->firstName != NULL) {
                RTICdrType_copyStringEx(
                    &sample->firstName,
                    "",
                    ((MAX_STR_SIZE)),
                    RTI_FALSE);
                if (sample->firstName == NULL) {
                    return RTI_FALSE;
                }
            }
        }

    }
    if (!allocParams->allocate_optional_members) {
        sample->lastName=NULL;
    } else {   

        if (allocParams->allocate_memory) {
            sample->lastName = DDS_String_alloc(((MAX_STR_SIZE)));
            RTICdrType_copyStringEx(
                &sample->lastName,
                "",
                ((MAX_STR_SIZE)),
                RTI_FALSE);
            if (sample->lastName == NULL) {
                return RTI_FALSE;
            }
        } else {
            if (sample->lastName != NULL) {
                RTICdrType_copyStringEx(
                    &sample->lastName,
                    "",
                    ((MAX_STR_SIZE)),
                    RTI_FALSE);
                if (sample->lastName == NULL) {
                    return RTI_FALSE;
                }
            }
        }

    }
    return RTI_TRUE;
}

RTIBool ChatUser_finalize_w_return(
    ChatUser* sample)
{
    ChatUser_finalize_ex(sample, RTI_TRUE);

    return RTI_TRUE;
}

void ChatUser_finalize(
    ChatUser* sample)
{

    ChatUser_finalize_ex(sample,RTI_TRUE);
}

void ChatUser_finalize_ex(
    ChatUser* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    ChatUser_finalize_w_params(
        sample,&deallocParams);
}

void ChatUser_finalize_w_params(
    ChatUser* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }

    if (deallocParams == NULL) {
        return;
    }

    if (sample->username != NULL) {
        DDS_String_free(sample->username);
        sample->username=NULL;

    }
    if (sample->group != NULL) {
        DDS_String_free(sample->group);
        sample->group=NULL;

    }
    if (deallocParams->delete_optional_members) {
        if (sample->firstName != NULL) {
            DDS_String_free(sample->firstName);
            sample->firstName=NULL;

        }
    }
    if (deallocParams->delete_optional_members) {
        if (sample->lastName != NULL) {
            DDS_String_free(sample->lastName);
            sample->lastName=NULL;

        }
    }
}

void ChatUser_finalize_optional_members(
    ChatUser* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    if (sample->firstName != NULL) {
        DDS_String_free(sample->firstName);
        sample->firstName=NULL;

    }
    if (sample->lastName != NULL) {
        DDS_String_free(sample->lastName);
        sample->lastName=NULL;

    }
}

RTIBool ChatUser_copy(
    ChatUser* dst,
    const ChatUser* src)
{
    try {

        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = DDS_BOOLEAN_TRUE;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;    

        if (dst == NULL || src == NULL) {
            return RTI_FALSE;
        }

        if (!RTICdrType_copyStringEx (
            &dst->username, src->username, 
            ((MAX_STR_SIZE)) + 1, RTI_FALSE)){
            return RTI_FALSE;
        }
        if (!RTICdrType_copyStringEx (
            &dst->group, src->group, 
            ((MAX_STR_SIZE)) + 1, RTI_FALSE)){
            return RTI_FALSE;
        }

        if (src->firstName!=NULL) {
            if (dst->firstName==NULL) {

                dst->firstName= DDS_String_alloc (((MAX_STR_SIZE)));
                if (dst->firstName == NULL) {
                    return RTI_FALSE;
                }       
            }

            if (!RTICdrType_copyStringEx (
                &dst->firstName, src->firstName, 
                ((MAX_STR_SIZE)) + 1, RTI_FALSE)){
                return RTI_FALSE;
            }
        } else {

            if (dst->firstName != NULL) {

                DDS_String_free(dst->firstName);   
                dst->firstName = NULL;     
            }   
        }

        if (src->lastName!=NULL) {
            if (dst->lastName==NULL) {

                dst->lastName= DDS_String_alloc (((MAX_STR_SIZE)));
                if (dst->lastName == NULL) {
                    return RTI_FALSE;
                }       
            }

            if (!RTICdrType_copyStringEx (
                &dst->lastName, src->lastName, 
                ((MAX_STR_SIZE)) + 1, RTI_FALSE)){
                return RTI_FALSE;
            }
        } else {

            if (dst->lastName != NULL) {

                DDS_String_free(dst->lastName);   
                dst->lastName = NULL;     
            }   
        }

        return RTI_TRUE;

    } catch (const std::bad_alloc&) {
        return RTI_FALSE;
    }
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'ChatUser' sequence class.
*/
#define T ChatUser
#define TSeq ChatUserSeq

#define T_initialize_w_params ChatUser_initialize_w_params

#define T_finalize_w_params   ChatUser_finalize_w_params
#define T_copy       ChatUser_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params

#undef T_initialize_w_params

#undef TSeq
#undef T

/* ========================================================================= */
const char *ChatMessageTYPENAME = "ChatMessage";

#ifndef NDDS_STANDALONE_TYPE
DDS_TypeCode* ChatMessage_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode ChatMessage_g_tc_user_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_STR_SIZE)));
    static DDS_TypeCode ChatMessage_g_tc_group_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_STR_SIZE)));
    static DDS_TypeCode ChatMessage_g_tc_message_string = DDS_INITIALIZE_STRING_TYPECODE(((MAX_MSG_SIZE)));

    static DDS_TypeCode_Member ChatMessage_g_tc_members[3]=
    {

        {
            (char *)"user",/* Member name */
            {
                0,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_KEY_MEMBER , /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }, 
        {
            (char *)"group",/* Member name */
            {
                1,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_KEY_MEMBER , /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }, 
        {
            (char *)"message",/* Member name */
            {
                2,/* Representation ID */
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER
        }
    };

    static DDS_TypeCode ChatMessage_g_tc =
    {{
            DDS_TK_STRUCT, /* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"ChatMessage", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            3, /* Number of members */
            ChatMessage_g_tc_members, /* Members */
            DDS_VM_NONE, /* Ignored */
            RTICdrTypeCodeAnnotations_INITIALIZER,
            DDS_BOOLEAN_TRUE, /* _isCopyable */
            NULL, /* _sampleAccessInfo: assigned later */
            NULL /* _typePlugin: assigned later */
        }}; /* Type code for ChatMessage*/

    if (is_initialized) {
        return &ChatMessage_g_tc;
    }

    ChatMessage_g_tc._data._annotations._allowedDataRepresentationMask = 5;

    ChatMessage_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&ChatMessage_g_tc_user_string;
    ChatMessage_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&ChatMessage_g_tc_group_string;
    ChatMessage_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&ChatMessage_g_tc_message_string;

    /* Initialize the values for member annotations. */
    ChatMessage_g_tc_members[0]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
    ChatMessage_g_tc_members[0]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

    ChatMessage_g_tc_members[1]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
    ChatMessage_g_tc_members[1]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

    ChatMessage_g_tc_members[2]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
    ChatMessage_g_tc_members[2]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

    ChatMessage_g_tc._data._sampleAccessInfo =
    ChatMessage_get_sample_access_info();
    ChatMessage_g_tc._data._typePlugin =
    ChatMessage_get_type_plugin_info();    

    is_initialized = RTI_TRUE;

    return &ChatMessage_g_tc;
}

#define TSeq ChatMessageSeq
#define T ChatMessage
#include "dds_cpp/generic/dds_cpp_data_TInterpreterSupport.gen"
#undef T
#undef TSeq

RTIXCdrSampleAccessInfo *ChatMessage_get_sample_seq_access_info()
{
    static RTIXCdrSampleAccessInfo ChatMessage_g_seqSampleAccessInfo = {
        RTI_XCDR_TYPE_BINDING_CPP, \
        {sizeof(ChatMessageSeq),0,0,0}, \
        RTI_XCDR_FALSE, \
        DDS_Sequence_get_member_value_pointer, \
        ChatMessageSeq_set_member_element_count, \
        NULL, \
        NULL, \
        NULL \
    };

    return &ChatMessage_g_seqSampleAccessInfo;
}

RTIXCdrSampleAccessInfo *ChatMessage_get_sample_access_info()
{
    static RTIBool is_initialized = RTI_FALSE;

    ChatMessage *sample;

    static RTIXCdrMemberAccessInfo ChatMessage_g_memberAccessInfos[3] =
    {RTIXCdrMemberAccessInfo_INITIALIZER};

    static RTIXCdrSampleAccessInfo ChatMessage_g_sampleAccessInfo = 
    RTIXCdrSampleAccessInfo_INITIALIZER;

    if (is_initialized) {
        return (RTIXCdrSampleAccessInfo*) &ChatMessage_g_sampleAccessInfo;
    }

    RTIXCdrHeap_allocateStruct(
        &sample, 
        ChatMessage);
    if (sample == NULL) {
        return NULL;
    }

    ChatMessage_g_memberAccessInfos[0].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->user - (char *)sample);

    ChatMessage_g_memberAccessInfos[1].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->group - (char *)sample);

    ChatMessage_g_memberAccessInfos[2].bindingMemberValueOffset[0] = 
    (RTIXCdrUnsignedLong) ((char *)&sample->message - (char *)sample);

    ChatMessage_g_sampleAccessInfo.memberAccessInfos = 
    ChatMessage_g_memberAccessInfos;

    {
        size_t candidateTypeSize = sizeof(ChatMessage);

        if (candidateTypeSize > RTIXCdrUnsignedLong_MAX) {
            ChatMessage_g_sampleAccessInfo.typeSize[0] =
            RTIXCdrUnsignedLong_MAX;
        } else {
            ChatMessage_g_sampleAccessInfo.typeSize[0] =
            (RTIXCdrUnsignedLong) candidateTypeSize;
        }
    }

    ChatMessage_g_sampleAccessInfo.useGetMemberValueOnlyWithRef =
    RTI_XCDR_TRUE;

    ChatMessage_g_sampleAccessInfo.getMemberValuePointerFcn = 
    ChatMessage_get_member_value_pointer;

    ChatMessage_g_sampleAccessInfo.languageBinding = 
    RTI_XCDR_TYPE_BINDING_CPP ;

    RTIXCdrHeap_freeStruct(sample);
    is_initialized = RTI_TRUE;
    return (RTIXCdrSampleAccessInfo*) &ChatMessage_g_sampleAccessInfo;
}

RTIXCdrTypePlugin *ChatMessage_get_type_plugin_info()
{
    static RTIXCdrTypePlugin ChatMessage_g_typePlugin = 
    {
        NULL, /* serialize */
        NULL, /* serialize_key */
        NULL, /* deserialize_sample */
        NULL, /* deserialize_key_sample */
        NULL, /* skip */
        NULL, /* get_serialized_sample_size */
        NULL, /* get_serialized_sample_max_size_ex */
        NULL, /* get_serialized_key_max_size_ex */
        NULL, /* get_serialized_sample_min_size */
        NULL, /* serialized_sample_to_key */
        (RTIXCdrTypePluginInitializeSampleFunction) 
        ChatMessage_initialize_ex,
        NULL,
        (RTIXCdrTypePluginFinalizeSampleFunction)
        ChatMessage_finalize_w_return,
        NULL
    };

    return &ChatMessage_g_typePlugin;
}
#endif

RTIBool ChatMessage_initialize(
    ChatMessage* sample) {
    return ChatMessage_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool ChatMessage_initialize_ex(
    ChatMessage* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return ChatMessage_initialize_w_params(
        sample,&allocParams);

}

RTIBool ChatMessage_initialize_w_params(
    ChatMessage* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (sample == NULL) {
        return RTI_FALSE;
    }
    if (allocParams == NULL) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory) {
        sample->user = DDS_String_alloc(((MAX_STR_SIZE)));
        RTICdrType_copyStringEx(
            &sample->user,
            "",
            ((MAX_STR_SIZE)),
            RTI_FALSE);
        if (sample->user == NULL) {
            return RTI_FALSE;
        }
    } else {
        if (sample->user != NULL) {
            RTICdrType_copyStringEx(
                &sample->user,
                "",
                ((MAX_STR_SIZE)),
                RTI_FALSE);
            if (sample->user == NULL) {
                return RTI_FALSE;
            }
        }
    }

    if (allocParams->allocate_memory) {
        sample->group = DDS_String_alloc(((MAX_STR_SIZE)));
        RTICdrType_copyStringEx(
            &sample->group,
            "",
            ((MAX_STR_SIZE)),
            RTI_FALSE);
        if (sample->group == NULL) {
            return RTI_FALSE;
        }
    } else {
        if (sample->group != NULL) {
            RTICdrType_copyStringEx(
                &sample->group,
                "",
                ((MAX_STR_SIZE)),
                RTI_FALSE);
            if (sample->group == NULL) {
                return RTI_FALSE;
            }
        }
    }

    if (allocParams->allocate_memory) {
        sample->message = DDS_String_alloc(((MAX_MSG_SIZE)));
        RTICdrType_copyStringEx(
            &sample->message,
            "",
            ((MAX_MSG_SIZE)),
            RTI_FALSE);
        if (sample->message == NULL) {
            return RTI_FALSE;
        }
    } else {
        if (sample->message != NULL) {
            RTICdrType_copyStringEx(
                &sample->message,
                "",
                ((MAX_MSG_SIZE)),
                RTI_FALSE);
            if (sample->message == NULL) {
                return RTI_FALSE;
            }
        }
    }

    return RTI_TRUE;
}

RTIBool ChatMessage_finalize_w_return(
    ChatMessage* sample)
{
    ChatMessage_finalize_ex(sample, RTI_TRUE);

    return RTI_TRUE;
}

void ChatMessage_finalize(
    ChatMessage* sample)
{

    ChatMessage_finalize_ex(sample,RTI_TRUE);
}

void ChatMessage_finalize_ex(
    ChatMessage* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    ChatMessage_finalize_w_params(
        sample,&deallocParams);
}

void ChatMessage_finalize_w_params(
    ChatMessage* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }

    if (deallocParams == NULL) {
        return;
    }

    if (sample->user != NULL) {
        DDS_String_free(sample->user);
        sample->user=NULL;

    }
    if (sample->group != NULL) {
        DDS_String_free(sample->group);
        sample->group=NULL;

    }
    if (sample->message != NULL) {
        DDS_String_free(sample->message);
        sample->message=NULL;

    }
}

void ChatMessage_finalize_optional_members(
    ChatMessage* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool ChatMessage_copy(
    ChatMessage* dst,
    const ChatMessage* src)
{
    try {

        if (dst == NULL || src == NULL) {
            return RTI_FALSE;
        }

        if (!RTICdrType_copyStringEx (
            &dst->user, src->user, 
            ((MAX_STR_SIZE)) + 1, RTI_FALSE)){
            return RTI_FALSE;
        }
        if (!RTICdrType_copyStringEx (
            &dst->group, src->group, 
            ((MAX_STR_SIZE)) + 1, RTI_FALSE)){
            return RTI_FALSE;
        }
        if (!RTICdrType_copyStringEx (
            &dst->message, src->message, 
            ((MAX_MSG_SIZE)) + 1, RTI_FALSE)){
            return RTI_FALSE;
        }

        return RTI_TRUE;

    } catch (const std::bad_alloc&) {
        return RTI_FALSE;
    }
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'ChatMessage' sequence class.
*/
#define T ChatMessage
#define TSeq ChatMessageSeq

#define T_initialize_w_params ChatMessage_initialize_w_params

#define T_finalize_w_params   ChatMessage_finalize_w_params
#define T_copy       ChatMessage_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params

#undef T_initialize_w_params

#undef TSeq
#undef T

#ifndef NDDS_STANDALONE_TYPE
namespace rti { 
    namespace xcdr {
        const RTIXCdrTypeCode * type_code<ChatUser>::get() 
        {
            return (const RTIXCdrTypeCode *) ChatUser_get_typecode();
        }

        const RTIXCdrTypeCode * type_code<ChatMessage>::get() 
        {
            return (const RTIXCdrTypeCode *) ChatMessage_get_typecode();
        }

    } 
}
#endif
