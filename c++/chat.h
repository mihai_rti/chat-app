

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from chat.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef chat_1436264753_h
#define chat_1436264753_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#include "rti/xcdr/Interpreter.hpp"
#else
#include "ndds_standalone_type.h"
#endif

static const DDS_Long MAX_STR_SIZE= 128;
static const DDS_Long MAX_MSG_SIZE= 256;
static const DDS_Char * const user_topic_name= "user";
static const DDS_Char * const message_topic_name= "message";
extern "C" {

    extern const char *ChatUserTYPENAME;

}

struct ChatUserSeq;
#ifndef NDDS_STANDALONE_TYPE
class ChatUserTypeSupport;
class ChatUserDataWriter;
class ChatUserDataReader;
#endif
class ChatUser 
{
  public:
    typedef struct ChatUserSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef ChatUserTypeSupport TypeSupport;
    typedef ChatUserDataWriter DataWriter;
    typedef ChatUserDataReader DataReader;
    #endif

    DDS_Char *   username ;
    DDS_Char *   group ;
    DDS_Char *   firstName ;
    DDS_Char *   lastName ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

#ifndef NDDS_STANDALONE_TYPE
NDDSUSERDllExport DDS_TypeCode* ChatUser_get_typecode(void); /* Type code */
NDDSUSERDllExport RTIXCdrTypePlugin *ChatUser_get_type_plugin_info(void);
NDDSUSERDllExport RTIXCdrSampleAccessInfo *ChatUser_get_sample_access_info(void);
NDDSUSERDllExport RTIXCdrSampleAccessInfo *ChatUser_get_sample_seq_access_info(void);
#endif

DDS_SEQUENCE(ChatUserSeq, ChatUser);

NDDSUSERDllExport
RTIBool ChatUser_initialize(
    ChatUser* self);

NDDSUSERDllExport
RTIBool ChatUser_initialize_ex(
    ChatUser* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool ChatUser_initialize_w_params(
    ChatUser* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
RTIBool ChatUser_finalize_w_return(
    ChatUser* self);

NDDSUSERDllExport
void ChatUser_finalize(
    ChatUser* self);

NDDSUSERDllExport
void ChatUser_finalize_ex(
    ChatUser* self,RTIBool deletePointers);

NDDSUSERDllExport
void ChatUser_finalize_w_params(
    ChatUser* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void ChatUser_finalize_optional_members(
    ChatUser* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool ChatUser_copy(
    ChatUser* dst,
    const ChatUser* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif
extern "C" {

    extern const char *ChatMessageTYPENAME;

}

struct ChatMessageSeq;
#ifndef NDDS_STANDALONE_TYPE
class ChatMessageTypeSupport;
class ChatMessageDataWriter;
class ChatMessageDataReader;
#endif
class ChatMessage 
{
  public:
    typedef struct ChatMessageSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef ChatMessageTypeSupport TypeSupport;
    typedef ChatMessageDataWriter DataWriter;
    typedef ChatMessageDataReader DataReader;
    #endif

    DDS_Char *   user ;
    DDS_Char *   group ;
    DDS_Char *   message ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

#ifndef NDDS_STANDALONE_TYPE
NDDSUSERDllExport DDS_TypeCode* ChatMessage_get_typecode(void); /* Type code */
NDDSUSERDllExport RTIXCdrTypePlugin *ChatMessage_get_type_plugin_info(void);
NDDSUSERDllExport RTIXCdrSampleAccessInfo *ChatMessage_get_sample_access_info(void);
NDDSUSERDllExport RTIXCdrSampleAccessInfo *ChatMessage_get_sample_seq_access_info(void);
#endif

DDS_SEQUENCE(ChatMessageSeq, ChatMessage);

NDDSUSERDllExport
RTIBool ChatMessage_initialize(
    ChatMessage* self);

NDDSUSERDllExport
RTIBool ChatMessage_initialize_ex(
    ChatMessage* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool ChatMessage_initialize_w_params(
    ChatMessage* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
RTIBool ChatMessage_finalize_w_return(
    ChatMessage* self);

NDDSUSERDllExport
void ChatMessage_finalize(
    ChatMessage* self);

NDDSUSERDllExport
void ChatMessage_finalize_ex(
    ChatMessage* self,RTIBool deletePointers);

NDDSUSERDllExport
void ChatMessage_finalize_w_params(
    ChatMessage* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void ChatMessage_finalize_optional_members(
    ChatMessage* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool ChatMessage_copy(
    ChatMessage* dst,
    const ChatMessage* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#ifndef NDDS_STANDALONE_TYPE
namespace rti { 
    namespace xcdr {
        template <>
        struct type_code<ChatUser> {
            static const RTIXCdrTypeCode * get();
        };

        template <>
        struct type_code<ChatMessage> {
            static const RTIXCdrTypeCode * get();
        };

    } 
}

#endif

#endif /* chat */

