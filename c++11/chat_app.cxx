#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <unordered_map>

using namespace std;

#include <dds/dds.hpp>
#include <dds/core/ddscore.hpp>
#include <rti/util/util.hpp>
#include <rti/core/ListenerBinder.hpp>
#include "chat.hpp"

dds::core::QosProvider qos_provider = dds::core::null;
dds::domain::DomainParticipant participant = dds::core::null;

struct UserInfo {
    string group;
    string fullName;
};
unordered_map<string, UserInfo> online_users;
mutex participant_mtx;
mutex map_mtx;

dds::core::cond::GuardCondition stop_condition;


int publisher_userInfo(dds::topic::Topic<ChatUser> userInfo_topic, 
    string username, string group, string firstName, string lastName)
{
    cout << "___Publisher_userInfo thread: started\n";

    participant_mtx.lock();
    dds::pub::DataWriter<ChatUser> user_writer(dds::pub::Publisher(participant), userInfo_topic, 
        qos_provider->datawriter_qos("chat_library::user_info_profile"));
    participant_mtx.unlock();

    ChatUser instance;
    instance.username(username);
    instance.group(group);
    instance.firstName(firstName);
    instance.lastName(lastName);

    user_writer.write(instance);

    // wake up based on guard condition
    while (true) {
        rti::util::sleep(dds::core::Duration(5));
        if (stop_condition.trigger_value() == true) {
            user_writer.unregister_instance(user_writer.register_instance(instance));
            break;
        }
    }
    cout << "___Publisher_userInfo thread: finished\n";
    return 0;
}


int publisher_main(dds::topic::Topic<ChatMessage> message_topic, string username, string group)
{
    cout << "___Publisher_main thread: started\n";

    participant_mtx.lock();
    // create partition for group
    dds::pub::qos::PublisherQos publisher_qos = dds::core::QosProvider::Default().publisher_qos();
    dds::core::policy::Partition partition = publisher_qos.policy<dds::core::policy::Partition>();
    vector<string> partition_names = { group };
    partition.name(partition_names);
    publisher_qos << partition;

    dds::pub::Publisher publisher(participant, publisher_qos);
    dds::pub::DataWriter<ChatMessage> message_writer(publisher, message_topic,
        qos_provider->datawriter_qos("chat_library::message_profile"));
    participant_mtx.unlock();

    while (true) {
        string input, command;
        getline(cin, input);
        stringstream ss(input);
        getline(ss, command, ' ');

        if (command == "list") {
            string list;
            map_mtx.lock();
            for (auto& it : online_users)
                list += "# " + it.first + " " + it.second.group + " " + it.second.fullName + "\n";
            map_mtx.unlock();
            cout << list;
        
        } else if (command == "send") {
            string destination;
            string message;
            ChatMessage instance;

            getline(ss, destination, ' ');
            getline(ss, message, '\n');
            message = username + ": " + message;  
            instance.message(message);

            map_mtx.lock();
            // destination can be a user or a group
            if (online_users.count(destination))
                instance.user(destination);
            else
                instance.group(destination);
            map_mtx.unlock();

            // write
            message_writer.write(instance);

        } else if (command == "exit") {
            stop_condition.trigger_value(true);
            break;
        } else {
            cout << "___Unsupported command" << endl;
        }
    }
    cout << "___Publisher_main thread: finished\n";
    return 0;
}


class BuiltinParticipantListener : 
    public dds::sub::NoOpDataReaderListener<dds::topic::ParticipantBuiltinTopicData> {
public:
    void on_data_available(dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData>& reader)
    {
        // take not new view states only
        dds::sub::LoanedSamples<dds::topic::ParticipantBuiltinTopicData> samples = 
            reader.select().state(dds::sub::status::DataState::new_instance()).take();

        for (auto sampleIt = samples.begin(); sampleIt != samples.end(); sampleIt++) {
            if (sampleIt->info().valid()) {
                rti::core::policy::Property properties = sampleIt->data().extensions().property();
                string debug_message = "__D__Discovered ";
                debug_message += properties.get("dds.sys_info.username");
                debug_message += "@" + properties.get("dds.sys_info.hostname");
                debug_message += " running chat_app with PID " + properties.get("dds.sys_info.process_id");
                cout << debug_message << endl;
            }
        }
    }
};


int subscriber_userInfo(dds::topic::Topic<ChatUser> userInfo_topic)
{
    cout << "___Subscriber_userInfo thread: started\n";

    participant_mtx.lock();
    dds::sub::DataReader<ChatUser> user_reader(dds::sub::Subscriber(participant), userInfo_topic,
        qos_provider->datareader_qos("chat_library::user_info_profile"));
    participant_mtx.unlock();

    dds::sub::cond::ReadCondition read_condition(user_reader, dds::sub::status::DataState::any());

    dds::core::cond::WaitSet waitset;
    waitset.attach_condition(read_condition);
    waitset.attach_condition(stop_condition);

    while (true) {
        dds::core::cond::WaitSet::ConditionSeq active_conditions = waitset.wait(dds::core::Duration(60));
        if (active_conditions.size() == 0) {
            cout << "Wait timed out!! No conditions were triggered." << endl;
            continue;
        }
        for (vector<int>::size_type c = 0; c < active_conditions.size(); c++) {
            if (active_conditions[c] == read_condition) {

                dds::sub::LoanedSamples<ChatUser> samples = user_reader.take();
                for (auto sampleIt = samples.begin(); sampleIt != samples.end(); sampleIt++) {
                    if (sampleIt->info().valid()) {
                        UserInfo ui;
                        ui.group = sampleIt->data().group();
                        ui.fullName = sampleIt->data().firstName() + " " + sampleIt->data().lastName();
                        // Add user to online_users
                        map_mtx.lock();
                        online_users[sampleIt->data().username()] = ui;
                        map_mtx.unlock();
                        cout << sampleIt->data().username() << " has entered the chat" << endl;
                    } else {
                        if (sampleIt->info().state().instance_state() != dds::sub::status::InstanceState::alive()) {
                            ChatUser cu;
                            user_reader.key_value(cu, sampleIt->info().instance_handle());
                            string username = cu.username();
                            cout << username + " has left the chat" << endl;
                            map_mtx.lock();
                            online_users.erase(username);
                            map_mtx.unlock();
                        }
                    }
                }
            } else if (active_conditions[c] == stop_condition) {
                return 0;
            }
        }
    }
    cout << "___Subscriber_userInfo thread: finished\n";
    return 0;
}


int subscriber_main(dds::topic::Topic<ChatMessage> message_topic, string username, string group)
{
    cout << "___Subscriber_main thread: started\n";

    vector<string> parameters = { "'" + username + "'", "'" + group + "'" };
    dds::topic::ContentFilteredTopic<ChatMessage> cft(message_topic, "Chat_CFT", dds::topic::Filter(
        "user MATCH %0 or group MATCH %1", parameters));
    
    participant_mtx.lock();
    // create partition for group
    dds::sub::qos::SubscriberQos subscriber_qos = dds::core::QosProvider::Default().subscriber_qos();
    dds::core::policy::Partition partition = subscriber_qos.policy<dds::core::policy::Partition>();
    vector<string> partition_names = { group };
    partition.name(partition_names);
    subscriber_qos << partition;

    dds::sub::Subscriber subscriber(participant, subscriber_qos);
    dds::sub::DataReader<ChatMessage> message_reader(subscriber, cft,
        qos_provider->datareader_qos("chat_library::message_profile"));
    participant_mtx.unlock();

    dds::sub::cond::ReadCondition read_condition(message_reader, dds::sub::status::DataState::any());

    dds::core::cond::WaitSet waitset;
    waitset.attach_condition(read_condition);
    waitset.attach_condition(stop_condition);

    while (true) {
        dds::core::cond::WaitSet::ConditionSeq active_conditions = waitset.wait(dds::core::Duration(60));
        if (active_conditions.size() == 0) {
            cout << "Wait timed out!! No conditions were triggered." << endl;
            continue;
        }
        for (vector<int>::size_type c = 0; c < active_conditions.size(); c++) {
            if (active_conditions[c] == read_condition) {           
                dds::sub::LoanedSamples<ChatMessage> samples = message_reader.take();
                for (auto sampleIt = samples.begin(); sampleIt != samples.end(); sampleIt++)
                    if (sampleIt->info().valid())
                        cout << sampleIt->data().message() << endl;
            } else if (active_conditions[c] == stop_condition) {
                return 0;
            }
        }
    }
    cout << "___Subscriber_main thread: finished\n";
    return 0;
}


int main(int argc, char* argv[])
{
    string username, group;
    string firstName, lastName;
    const int domain_id = 10;

    if (argc < 3) {
        cerr << "Usage: chat_publisher username group [firstName] [lastName]" << endl;
        return -1;
    }
    username = string(argv[1]);
    group = string(argv[2]);
    if (argc >= 4)
        firstName = string(argv[3]);
    if (argc >= 5)
        lastName = string(argv[4]);

    try {
        // create global domain participant
        qos_provider = dds::core::QosProvider("chat_qos.xml");
        participant = dds::domain::DomainParticipant(domain_id, qos_provider->participant_qos("chat_library::chat_profile"));

        dds::topic::Topic<ChatUser> userInfo_topic(participant, user_topic_name);
        dds::topic::Topic<ChatMessage> message_topic(participant, message_topic_name);

        // add listener to builtin topic for debug info
        dds::sub::Subscriber builtin_subscriber = dds::sub::builtin_subscriber(participant);
        // lookup datareader
        vector< dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> > participant_reader;
        dds::sub::find< dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> >(builtin_subscriber,
            dds::topic::participant_topic_name(), std::back_inserter(participant_reader));
        // install listener using ListenerBinder
        rti::core::ListenerBinder< dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> > participant_listener =
            rti::core::bind_and_manage_listener(participant_reader[0],
                new BuiltinParticipantListener, dds::core::status::StatusMask::data_available());
        // enable participant after listener is installed
        participant.enable();

        // create subscriber for user topic
        thread subscriber_thread_user_info(subscriber_userInfo, userInfo_topic);
        // create subscriber for message topic
        thread subscriber_thread_message(subscriber_main, message_topic, username, group);
        // create publisher for user topic
        thread publisher_thread_user_info(publisher_userInfo, userInfo_topic, username, group, firstName, lastName);

        // create subscriber for message topic
        publisher_main(message_topic, username, group);

        // wait for threads to finish
        publisher_thread_user_info.join();
        subscriber_thread_user_info.join();
        subscriber_thread_message.join();

    } catch (const exception& ex) {
        cout << "Exception caught: " << ex.what() << endl;
    }

    return 0;
}
